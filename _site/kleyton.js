window.onload=function () { setInterval(moveSliderElements,5000); };

var divElements = document.querySelectorAll("#slider div.sliderElement");
var quantityOfShownElements=4;
var contSlider=0;
var sliderNextElementPosition=4;//((divElements.length)/(divElements.length/quantityOfShownElements))
var sliderElementWidth=12.5;//This controls the all the width of the elements from the slider
var sliderElementMarginLeft=0;//This controls the all the marginLeft of the elements from the slider
var cont=0;
var sliderNextElementPositionPOPULAR=4;

function moveSliderElements() {
    var i;
    divElements = document.querySelectorAll("#slider div.sliderElement");
    for (i=0;i<divElements.length;i++){
        divElements[i].style.transition="margin 3s, opacity 3s, transform 2s, filter 2s, box-shadow 2s";
    }
    if (contSlider<(divElements.length-quantityOfShownElements)){
        if(contSlider>0) divElements[contSlider-1].style.display="none";
        divElements[contSlider].style.opacity="0";
        divElements[contSlider].style.marginLeft=(((sliderElementWidth+sliderElementMarginLeft)*(-1))+"%");//this value must be the result of the width of the element more the margin between the element multiplied per minus 1:[(10+0.3)*(-1)]
        if (sliderNextElementPosition<divElements.length-1) divElements[sliderNextElementPosition+1].style.display="inline-block";
        divElements[sliderNextElementPosition].style.display="inline-block";
        divElements[sliderNextElementPosition].style.opacity="1";//caution, in css file this object is daclared as opcity=0;
        contSlider++;
        sliderNextElementPosition++;
    }else {

        for (i=0;i<divElements.length;i++){
            divElements[i].style.transition="none";
            if (i<quantityOfShownElements){
                divElements[i].style.marginLeft=sliderElementMarginLeft+"%";
                divElements[i].style.display="inline-block";
                divElements[i].style.opacity="1";
            }else {
                divElements[i].style.display="none";
                divElements[i].style.opacity="0";
            }
        }
        for (i=4;i<divElements.length;i++){
            divElements[i].style.marginLeft=sliderElementMarginLeft+"%";
        }
        for (i=0;i<divElements.length;i++){
            divElements[i].style.transition="margin 0s, opacity 3s, transform 2s, filter 2s, box-shadow 2s";
        }

        contSlider=0;
        sliderNextElementPosition=4;
    }

}

function showSliderElementsButton(value) {

        for (i=0;i<divElements.length;i++){
            divElements[i].style.marginLeft=sliderElementMarginLeft+"%";
            divElements[i].style.transition="none";
            divElements[i].style.opacity="0";
            divElements[i].style.display="none";
            divElements[i].style.transition="margin 0s, opacity 3s, transform 2s, filter 2s, box-shadow 2s";
        }
        for (i=(quantityOfShownElements*value);(i<((quantityOfShownElements*value)+quantityOfShownElements));i++){
            divElements[i].style.marginLeft=sliderElementMarginLeft+"%";
            divElements[i].style.display="inline-block";
            divElements[i].style.opacity="1";
            divElements[i].style.transition="margin 3s, opacity 3s, transform 2s, filter 2s, box-shadow 2s";

        }
        contSlider=(quantityOfShownElements*value);
        sliderNextElementPosition=((quantityOfShownElements*value)+quantityOfShownElements);
}

function showSliderElementsButtonPOPULAR(value) {

        var divElementsPOPULAR = document.querySelectorAll("#sliderPOPULAR div.sliderElementPOPULAR");

        if (value==1){
            divElementsPOPULAR[cont].style.display="none";
            cont++;
            if (cont>=(divElementsPOPULAR.length-quantityOfShownElements)) {
                for (i=0;i<divElementsPOPULAR.length;i++){
                    divElementsPOPULAR[i].style.display="none";
                }
                for (i=0;i<sliderNextElementPositionPOPULAR;i++){
                    divElementsPOPULAR[i].style.display="inline-block";
                }
                cont=0;
            }else divElementsPOPULAR[sliderNextElementPositionPOPULAR+cont].style.display="inline-block";

        }else{
            divElementsPOPULAR[sliderNextElementPositionPOPULAR+cont].style.display="none";
            cont--;
            if(cont<0){
                for (i=0;i<divElementsPOPULAR.length;i++){
                    divElementsPOPULAR[i].style.display="none";
                }
                for (i=sliderNextElementPositionPOPULAR;i<divElementsPOPULAR.length;i++){
                    divElementsPOPULAR[i].style.display="inline-block";
                }
                cont=quantityOfShownElements-1;
            }else divElementsPOPULAR[cont].style.display="inline-block";

        }

}
