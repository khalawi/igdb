---
layout : videogame
name: "Spyro Reignited Trilogy"
franchise: "Spyro the Dragon"
description: "Spyro Reignited Trilogy contains remakes of the first three games in the Spyro series; all of which were originally developed by Insomniac Games for the PlayStation and released from 1998 to 2000. It was announced on April 5, 2018, following the release of Crash Bandicoot N. Sane Trilogy, which Activision also published.Eurogamer noted there was a high public demand for Spyro to receive the same treatment after N. Sane Trilogy was released.

Tom Kenny, who voiced Spyro the Dragon in the original Ripto's Rage! and Year of the Dragon, reprised his role in Spyro Reignited Trilogy with re-recorded voice lines, including the first game, in which Spyro was originally voiced by Carlos Alazraqui.Stewart Copeland, the music composer of the three original games, wrote a new main theme for the compilation. His original compositions for the games were remastered by Stephan Vankov, an employee of Toys for Bob, with the game including an option to choose between the two soundtracks. Reignited Trilogy uses the Unreal Engine 4 game engine."
genre: "Adventure"
theme: "Temazo"
rating : 10
platform: "Playstation 4-Xbox One"
release: "Nov 13, 2018 - PlayStation 4 
| Nov 13, 2018 - Xbox One"
year : 2018
developer_id: "toys for bob"
publisher: "Activision"
publisherUrl :  "https://www.activision.com/"
official: "https://spyrothedragon.com/es"
shop: "https://www.amazon.com/dp/B07BX4CVXG?tag=ps-testtag06&ascsubtag=wtbs_5ce9c7d8598dac6cc91f95fd&m=ATVPDKIKX0DER"
trailer: <iframe width="560" height="315" src="https://www.youtube.com/embed/mRc2MHS6owQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
image : "img/game1.jpg"
---
