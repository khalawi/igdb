---
layout : videogame
name : "Animal Crossing: New Leaf"
genre : "simulator"
rating : 10
developer : "Nintendo"
image : "img/acnewleaf.jpg"
release : 2013
description : "New life, new Animal Crossing"
---
