---
layout : videogame
name : "Star Craft 2"
description : "StarCraft II: Wings of Liberty is a science fiction real-time strategy video game developed and published by Blizzard Entertainment. It was released worldwide in July 2010 for Microsoft Windows and Mac OS X.[6] A sequel to the 1998 video game StarCraft and its expansion set Brood War, the game is split into three installments: the base game with the subtitle Wings of Liberty, an expansion pack Heart of the Swarm, and a stand-alone expansion pack Legacy of the Void.[7] In March of 2016, a campaign pack called StarCraft II: Nova Covert Ops was also released.

The game revolves around three species: the Terrans, human exiles from Earth; the Zerg, a super-species of assimilated life forms;[8] and the Protoss, a technologically advanced species with vast psionic powers. Wings of Liberty focuses on the Terrans, while the expansions Heart of the Swarm and Legacy of the Void focus on the Zerg and Protoss, respectively. The game is set four years after the events of 1998's StarCraft: Brood War, and follows the exploits of Jim Raynor as he leads an insurgent group against the autocratic Terran Dominion. The game includes both new and returning characters and locations from the original game.

The game was met with critical acclaim, receiving an aggregated score of 93% from Metacritic. Similar to its predecessor, StarCraft II was praised for its engaging gameplay, as well as its introduction of new features and improved storytelling. The game was criticized for lacking features that existed in the original StarCraft game including LAN play and the ability to switch between multiplayer regions. At the time of its release, StarCraft II became the fastest-selling real-time strategy game, with over three million copies sold worldwide in the first month.[9]

During BlizzCon 2017, it was announced that StarCraft II would be re-branded as a free-to-play game going forward. This unlocked the Wings of Liberty campaign, multiplayer and other modes of the game for everyone.[10] The change was in line with Blizzard's vision going forward, that is, supporting the game with micro-transactions such as Skins, Co-op Commanders, Voice Packs and War Chests which already proved to be successful."
genre : "RTS"
rating : 8
franchise : "Wings of liberty"
developer_id: "Blizzard"
platform : "PC"
official : https://starcraft2.com/es-es/
shop :  https://eu.shop.battle.net/es-es/family/starcraft-ii
developer : "Blizzard Entertainment"
publisher : "Blizzard Entertainment"
pulisherURL : "https://www.blizzard.com/es-es/"
trailer : <iframe width="560" height="315" src="https://www.youtube.com/embed/aVtXac6if14" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
image : "img/img1.jpg"
release : 2019
year : 2019
---
