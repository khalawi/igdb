---
layout : videogame
name : "For Honor"
description : "For Honor is an action fighting game set during a medieval, fantasy setting. Players can play as a character from three different factions, namely the Iron Legion (Knights), the Warborn (Vikings), and the Chosen (Samurai) . A fourth faction, the Wu Lin, was added in the Marching Fire expansion in October 2018. The four factions represent knights, vikings, samurai, and exiles respectively.Each hero also has quotes in their own languages that will trigger when certain actions are performed. The Knights speak in Latin, the Vikings speak Icelandic, the Samurai speak Japanese, and the Wu Lin speak Mandarin. Each faction had four classes at launch, with two more being added at the beginning of every season of the Faction War. The Vanguard class is described as well-balanced and has excellent offense and defense. The Assassin class is fast and efficient in dueling enemies, but the class deals much less damage to multiple enemies. The Heavies (Tanks) are more resistant to damages and are suitable for holding capture points, though their attacks are slow. The last class, known as Hybrid, is a combination of two of the three types, and is capable of using uncommon skills.

All heroes are unique and have their own weapons, skills, and fighting styles.Players fight against their opponents with their class-specific melee weapons. When players perform certain actions, such as killing multiple enemies consecutively, they gain Feats, which are additional perks. These perks allow players to gain additional points and strengths, call in a barrage of arrows or a catapult attack, or heal themselves.In most missions, players are accompanied by numerous AI minions. They are significantly weaker than the player character, and do not pose much threat."
genre : "Action"
rating : 10
developer_id: "Ubisoft"
platform : "PC, PS4, Xbox one"
official : 
shop :  
developer : "Ubisoft"
publisher : "Ubisoft"
pulisherURL : 
trailer : 
image : "img/img4.jpg"
release :  February 2017
year : 2017
---