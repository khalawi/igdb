---
layout : videogame
name : "Darsiders 2"
description : "Players take control of Death, one of the Four Horsemen of the Apocalypse. The core gameplay is an action role-playing hack and slash style. The game makes frequent use of interactive puzzle elements, requiring the player to think their way through a series of levers, doors, elevators, portals, etc. in order to traverse areas and reach objectives.
Maps are vast and each contain open world regions that can be explored freely on foot or by horse, along with numerous dungeons where quest objectives are generally carried out. There are main and side quests, with main and side boss fights. Worlds can be traversed via fast travel, whereby the player can teleport to certain map points instantly. While inside a dungeon, the player is allowed to fast travel back to the overworld while saving their dungeon location for continuation later without losing progress.
Death is aided by Despair, a horse that is available for use in open areas of the overworld for faster travel, and Dust, a raven that guides him to his objectives. Death's primary weapons are two scythes, one wielded in each hand. Secondary weapons include melee weapons like hammers, axes, and maces as slow options; fast options are generally gauntlet-style weapons that provide the player with claws and other bladed arm extensions, at the expense of less range and power than the slow weapons.
There are several different movement options, including swimming, running along walls, and climbing options that are available on specially placed wooden elements, such as wall pegs and beams. In the course of quests, Death acquires Death Grip, which operates as a grappling hook on certain objects; and Voidwalker. Other abilities like Soul Splitter and Interdiction are acquired, which allow the player to control multiple characters to traverse puzzle areas.
Health, Wrath, and Reaper resource meters display on-screen whenever they change, along with an experience meter that shows how close the player is to the next character class level. Wrath is the game's mana-type system, being a resource used for special abilities. Reaper is a separate resource used for the Reaper ability, and when full, Death can transform briefly into his grim reaper form, which is more resilient and deals more damage.
There are eight player statistics, including a character class level that increases at various experience levels. Each new level gives the player a skill point that can be used in a skill tree that contains new abilities. Other statistics can be increased by equipping items, with each item having various stat-altering characteristics. The player's inventory contains seven different pages of equipment classes (primary and secondary weapon, shoulder, armor, glove, boot, and talisman, with an additional page for quest items). New equipment can be acquired via enemy drops, looting chests, or purchasing from vendor characters. New combo moves can also be purchased from Trainer characters.
Stonebites, which are colored stones hidden throughout the world, can be collected (after several quests have been completed) and traded to a character named Blackroot, in groups of three, in exchange for various permanent statistic upgrades. There are three Stonebite types, indicated by their color, and the particular combination traded determines which upgrade is received.

Money is dropped by enemies and chests, and can be acquired by selling items to vendors. Special Possessed weapons are rarely acquired, which provide another more unorthodox mechanic for trading in unwanted items, whereby the possessed weapon can be upgraded by sacrificing other lesser items to it."
genre : "Adventure"
rating : 10
developer_id: "THQ"
platform : "PC, PS3, xbox360"
official : 
shop :  
developer : "THQ"
publisher : "THQ"
pulisherURL : 
trailer : 
image : "img/img7.jpg"
release :  November 2012
year : 2012
---