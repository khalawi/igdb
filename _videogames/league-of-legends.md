---
layout : videogame
name : "League of legends"
description : "League of Legends (abbreviated LoL) is a multiplayer online battle arena video game developed and published by Riot Games for Microsoft Windows and macOS. The game follows a freemium model and is supported by microtransactions, and was inspired by the Warcraft III: The Frozen Throne mod, Defense of the Ancients.

In League of Legends, players assume the role of an unseen summoner that controls a champion with unique abilities and battle against a team of other players or computer-controlled champions. The goal is usually to destroy the opposing team's Nexus, a structure that lies at the heart of a base protected by defensive structures, although other distinct game modes exist as well. Each League of Legends match is discrete, with all champions starting off fairly weak but increases in strength by accumulating items and experience over the course of the game.The champions and setting blend a variety of elements, including high fantasy, steampunk, and Lovecraftian horror.

League of Legends was generally well received upon its release in 2009, and has since grown in popularity, with an active and expansive fanbase. By July 2012, League of Legends was the most played PC game in North America and Europe in terms of the number of hours played In January 2014, over 67 million people played League of Legends per month, 27 million per day, and over 7.5 million concurrently during peak hours.[4] League has among the largest footprints of any game in streaming media communities on platforms such as YouTube and Twitch.tv; it routinely ranks first in the most-watched hours.In September 2016, the company estimated that there are over 100 million active players each month.The game's popularity has led it to expand into merchandise, with toys, accessories, apparel, as well as tie-ins to other media through music videos, web series, documentaries, and books.

League of Legends has an active and widespread competitive scene. In North America and Europe, Riot Games organizes the League Championship Series (LCS), located in Los Angeles and Berlin respectively, which consists of 10 professional teams in each continent.[9] Similar regional competitions exist in China (LPL), South Korea (LCK), Taiwan/Hong Kong/Macau (LMS), and various other regions. These regional competitions culminate with the annual World Championship. The 2017 World Championship had 60 million unique viewers and a total prize pool of over US$4 million.The 2018 Mid-Season Invitational had an overall peak concurrent viewership of 19.8 million, while the finals had an average concurrent viewership of 11 million."
genre : "Arena battle royal"
rating : 8
developer_id: "Riot Games"
platform : "PC"
official : "https://euw.leagueoflegends.com/en/"
shop :  
developer : "Riot Games"
publisher : "Riot Games"
pulisherURL : 
trailer : 
image : "img/img3.jpg"
release :  October 2019
year : 2019
---