---
layout : videogame
name : "Call of Duty 4"
description : Black Ops 4 is the first Call of Duty title without a traditional single-player campaign mode. Instead, it features the Solo Missions mode, which focuses on the backstories of the game's multiplayer characters, known as "Specialists". The missions take place between Black Ops II and III chronologically. Some of the Specialists also carried over from Black Ops III. The multiplayer mode is the first in the series to not feature automatic health regeneration and introduces both predictive recoil and a new ballistics system. The game included three Zombies maps on release day, four if a special edition of the game, or the Black Ops Pass, was purchased. The locations of the maps include the RMS Titanic, an arena in Ancient Rome, and Alcatraz Federal Penitentiary. The game also introduced a battle royale mode called Blackout, which features up to 100 players in each match. Many characters from this and other Black Ops titles can be used as the player's character model in this mode.
genre : "Action"
rating : 8
developer_id: "Activision"
platform : "PC, PS4, Xbox One"
official : https://www.callofduty.com/es/blackops4/
shop :  https://store.playstation.com/es-es/product/EP0002-CUSA12446_00-CODBO4DELUXE0001
developer : "Activision"
publisher : "Activision"
pulisherURL : 
trailer : <iframe width="560" height="315" src="https://www.youtube.com/embed/6kqe2ICmTxc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
image : "img/img6.jpg"
release :  October 12, 2018
year : 2018
---