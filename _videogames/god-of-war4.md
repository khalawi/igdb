---
layout : videogame
name : "God of War 4"
description : "God of War is an action-adventure video game developed by Santa Monica Studio and published by Sony Interactive Entertainment (SIE). Released on April 20, 2018, for the PlayStation 4 (PS4) console, it is the eighth installment in the God of War series, the eighth chronologically, and the sequel to 2010's God of War III. Unlike previous games, which were loosely based on Greek mythology, this installment is loosely based on Norse mythology, with the majority of it set in ancient Norway in the realm of Midgard. For the first time in the series, there are two protagonists: Kratos, the former Greek God of War who remains the only playable character, and his young son Atreus; at times, the player may passively control him. Following the death of Kratos' second wife and Atreus' mother, they journey to fulfill her promise to spread her ashes at the highest peak of the nine realms. Kratos keeps his troubled past a secret from Atreus, who is unaware of his divine nature. Along their journey, they encounter monsters and gods of the Norse world.

Described by creative director Cory Barlog as a reimagining of the franchise, a major gameplay change is that Kratos prominently uses a magical battle axe instead of his signature double-chained blades. God of War also uses an over-the-shoulder free camera, with the game in one shot, as opposed to the fixed cinematic camera of the previous entries. This was the first time a three-dimensional AAA game utilized a one-shot camera. The game also includes role-playing video game elements, and Kratos' son Atreus provides assistance in combat. The majority of the original game's development team worked on God of War and designed it to be accessible and grounded. A separate short text-based game, A Call from the Wilds, was released in February 2018 and follows Atreus on his first adventure.

God of War received universal acclaim for its narrative, world design, art direction, music, graphics, characters, and combat system. Many reviewers felt that it had successfully revitalized the series without losing the core identity of its predecessors. It received a number of perfect review scores, tying it with the original God of War (2005) as the highest-rated game in the series, as well as one of the highest-rated PlayStation 4 games of all time on the review aggregator Metacritic. The game performed well commercially, selling over five million copies within a month of release and over 10 million by May 2019, making it one of the best-selling PlayStation 4 games. Among other awards and nominations, God of War was awarded Game of the Year by numerous media outlets and award shows. A novelization was released in August 2018, followed by a four-issue prequel comic series published from November 2018 to February 2019."
genre : "Action"
rating : 10
developer_id: "Sony"
platform : "PS4"
official : 
shop :  
developer : "Sony"
publisher : "Sony"
pulisherURL : 
trailer : 
image : "img/img2.jpg"
release :  April 2018
year : 2018
---